Editor Experience Module
------------------------

Description
-----------
The Editor Experience module provides tools for enhancing the editor experience 
in Drupal. It aims to add simplicity when creating and editing content. It also 
provides an example .make file that contains existing contrib modules further 
enhance the editor experience in various ways.


Installation
------------
# Firstly install the modules from the .make file template (See section .make file 
template for more info on how to do that).
# Then Simply install the editor_experience module to your site. 


.make file template
-------------------
The module includes a .make file template with suggested module to enhance the 
editor experience. You can use the .make file as it is by just renaming it or copying the contents 
into your own .make file. Or, if you do not use a .make file for building your 
site, you can simply install the recommended modules from the .make file manually 
to your site. 


The Module
----------
The module do alterations to Drupal core and contrib modules included in the 
.make file template to improve the user experience of your site in various ways 
like:
# Add cancel link on node add/edit forms.
# Remove preview button for node and comment forms.
# Remove 'promote' and 'sticky' options on node edit forms.
