(function($) {

  Drupal.behaviors.editorExperienceMaxlengthHelper = {
    attach: function(context, settings) {

      if(CKEDITOR != undefined){
        CKEDITOR.on('instanceReady', function(e) {
          CKEDITOR.instances[e.editor.name].on('focus', Drupal.editorExperience.maxLengthfocusedToggle);
          CKEDITOR.instances[e.editor.name].on('blur', Drupal.editorExperience.maxLengthfocusedToggle);
        });
      }

    }
  }

  $.fn.charCount = function(options) {
    var options = $.extend(Drupal.settings.maxlength.defaults, options),
        ml = Drupal.maxlength;
    ml.options[$(this).attr('id')] = options;

    if (options.action == 'detach') {
      $(this).removeClass('maxlength-processed');
      $('#' + $(this).attr('id') + '-' + options.css).remove();
      delete ml.options[$(this).attr('id')];
      return 'removed';
    }

    var counterElement = $('<' + options.counterElement + ' id="' + $(this).attr('id') + '-' + options.css + '" class="' + options.css + '"></' + options.counterElement + '>');
    if ($(this).next('div.grippie').length) {
      $(this).next('div.grippie').after(counterElement);
    } else if($(this).is("textarea")){
      $(this).closest('.form-item').find('label').append(' - ').append(counterElement);
    } else {
      $(this).after(counterElement);
    }

    ml.calculate($(this), options);
    $(this).keyup(function() {
      ml.calculate($(this), options);
    });
    $(this).change(function() {
      ml.calculate($(this), options);
    });

    Drupal.maxlength.calculate($(this), options);
    $(this).keyup(function() {
      Drupal.maxlength.calculate($(this), options);
    });
    $(this).change(function() {
      Drupal.maxlength.calculate($(this), options);
    });

    ckeditorField = false;
    if(CKEDITOR != undefined){
      var id = $(this).attr('id');
      for(var i in Drupal.settings.wysiwyg.triggers){
        if(Drupal.settings.wysiwyg.triggers[i].field == id){
          ckeditorField = true;
        }
      }
    }

    if(!ckeditorField){
      $(this)
        .focus(Drupal.editorExperience.maxLengthfocusedToggle)
        .blur(Drupal.editorExperience.maxLengthfocusedToggle);
    }

  };

  Drupal.editorExperience = Drupal.editorExperience || {};

  Drupal.editorExperience.maxLengthfocusedToggle = function(e){
    if('mode' in this && this.mode == 'wysiwyg'  && 'name' in this){
      $currElement = $('#' + this.name);
      var eventType = e.name;
    }else{
      $currElement = $(this);
      var eventType = e.type;
    }
    if(eventType == 'focus'){
      $currElement.closest('.form-item').addClass('focused');
    }else if(eventType == 'blur'){
      $currElement.closest('.form-item').removeClass('focused');
    }
  };

})(jQuery);
